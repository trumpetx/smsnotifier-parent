package com.trumpetx.smsnotifier.controllers;

import com.trumpetx.smsnotifier.gui.MainStageAccessor;
import com.trumpetx.smsnotifier.services.SavedProperties;
import com.trumpetx.smsnotifier.services.TextManager;
import com.trumpetx.smsnotifier.util.javafx.PetiteFXMLFactory;
import javafx.fxml.Initializable;
import jodd.petite.meta.PetiteBean;
import jodd.petite.meta.PetiteInject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.net.URL;
import java.util.ResourceBundle;

public abstract class AbstractController implements Initializable, IPresentable {

    protected final Logger LOG = LoggerFactory.getLogger(getClass());
    @PetiteInject
    protected SavedProperties properties;
    @PetiteInject
    protected PetiteFXMLFactory petiteFXMLFactory;
    @PetiteInject
    protected MainStageAccessor mainStageAccessor;
    @PetiteInject
    protected TextManager textManager;

    @Override
    public void initialize(URL url, ResourceBundle bundle) {
        LOG.debug("Initialized: {}", url);
    }

    public void present() {
        mainStageAccessor.setScene(petiteFXMLFactory.getScene(view()));
    }

    protected String getText(String key) {
        return textManager.getText(key, new Object[0]);
    }

    protected String getText(String key, Object... params) {
        return textManager.getText(key, params);
    }

    public abstract String view();

}
