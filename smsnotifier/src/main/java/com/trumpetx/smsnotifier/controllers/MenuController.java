package com.trumpetx.smsnotifier.controllers;

import com.trumpetx.smsnotifier.util.PlatformUtil;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.MenuBar;
import javafx.stage.Stage;
import javafx.stage.StageStyle;
import jodd.petite.meta.PetiteBean;

import java.net.URL;
import java.util.ResourceBundle;

@PetiteBean
public class MenuController extends AbstractController {

    private Stage aboutStage;
    @FXML
    private MenuBar menuBar;

    @FXML
    private void exit(ActionEvent actionEvent) {
        LOG.debug("Exiting via menu item.");
        System.exit(0);
    }

    @FXML
    private void showAbout(ActionEvent actionEvent) {
        if (aboutStage != null) {
            LOG.debug("About window already open.");
            aboutStage.toFront();
            return;
        }
        LOG.debug("Showing About");
        aboutStage = new Stage(StageStyle.UTILITY);
        aboutStage.setResizable(false);
        aboutStage.setTitle(getText("about"));
        aboutStage.setScene(petiteFXMLFactory.getScene("about"));
        aboutStage.setOnCloseRequest(windowEvent -> {
            aboutStage.close();
            aboutStage = null;
        });
        aboutStage.show();
    }

    @Override
    public void initialize(URL url, ResourceBundle bundle) {
        super.initialize(url, bundle);
        if (PlatformUtil.isMac()) {
            menuBar.setUseSystemMenuBar(true);
        }
    }

    @Override
    public String view() {
        return "menu";
    }
}
