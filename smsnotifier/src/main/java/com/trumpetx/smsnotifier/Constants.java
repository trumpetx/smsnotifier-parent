package com.trumpetx.smsnotifier;

import java.util.*;

public class Constants {

    // Property Keys
    public static final String EMAIL_CFG = "email";
    public static final String WOWDIR_CFG = "wowDir";
    public static final String SMS_ENABLED_CFG = "sms.enabled";
    public static final String EMAIL_ENABLED_CFG = "email.enabled";
    public static final String DESKTOP_ENABLED_CFG = "desktop.enabled";
    public static final String PHONE_CFG = "phoneNumber";
    public static final String START_ON_STARTUP_CFG = "startup";
    public static final String SMS_VERIFIED_CFG = "verified";
    public static final String VERIFICATION_CODE_CFG = "verification.code";
    public static final String STARTUP_MINIMIZED_CFG = "startup.minimized";
    public static final String HIDE_ON_MINIMIZE_CFG = "hide.on.minimize";
    public static final String HIDE_ON_CLOSE_CFG = "hide.on.close";
    public static final String CARRIER_CFG = "phone.carrier";

    // Other Constants
    public static final String ATT = "AT&T";
    public static final String SPRINT = "Sprint";
    public static final String TMOBILE = "T-Mobile";
    public static final String VERIZON = "Verizon";
    public static final String OTHER = "Other";
    public static final List CARRIERS = Arrays.asList(ATT, SPRINT, TMOBILE, VERIZON, OTHER);
    public static final Map<String, String> CARRIER_EMAIL_TEMPLATES = Collections.unmodifiableMap(
            new HashMap<String, String>(){{
                put(ATT, "%s@txt.att.net");
                put(SPRINT, "%s@messaging.sprintpcs.com");
                put(TMOBILE, "%s@tmomail.net");
                put(VERIZON, "%s@vtext.com");
            }}
    );
}
