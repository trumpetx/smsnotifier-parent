package com.trumpetx.smsnotifier.runnable;

import com.trumpetx.smsnotifier.util.Validate;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.File;
import java.io.IOException;
import java.nio.file.*;
import java.nio.file.attribute.BasicFileAttributes;
import java.util.Arrays;
import java.util.List;

public abstract class WowFinder implements Runnable {

    private static final Logger LOG = LoggerFactory.getLogger(WowFinder.class);
    private static final String WOW_MAC_DEFAULT = "/Applications/World of Warcraft";
    private static final List<String> WOW_WIN_DEFAULT = Arrays.asList("C:\\Users\\Public\\Games\\World of Warcraft", "C:\\Program Files\\World of Warcraft", "C:\\Program Files (x86)\\World of Warcraft", "C:\\Users\\Public\\Documents\\Blizzard Entertainment", "C:\\World of Warcraft\\");

    private File wowFolder;

    @Override
    public void run() {
        if (System.getProperty("os.name").toLowerCase().contains("mac")) {
            wowFolder = new File(WOW_MAC_DEFAULT);
        } else if (System.getProperty("os.name").contains("win")) {
            for (String defaultLocation : WOW_WIN_DEFAULT) {
                wowFolder = new File(defaultLocation);
                if (Validate.readableDir( wowFolder )) {
                    callback(wowFolder);
                    return;
                }
            }
        }

        if (!Validate.readableDir( wowFolder )) {
            wowFolder = null;
            try {
                Files.walkFileTree(Paths.get(new File(File.separator).toURI()), new Finder());
            } catch (IOException e) {
                LOG.error("Error finding WoW directory", e);
            }
        }

        callback(wowFolder);
    }

    public abstract void callback(File wowFolder);

    private class Finder extends SimpleFileVisitor<Path> {

        private final PathMatcher matcher = FileSystems.getDefault().getPathMatcher("glob:World of Warcraft");

        @Override
        public FileVisitResult preVisitDirectory(Path dir,
                                                 BasicFileAttributes attrs) {
            Path name = dir.getFileName();
            if (name != null && matcher.matches(name)) {
                LOG.info("Found WoW directory: " + dir);
                wowFolder = dir.toFile();
                return FileVisitResult.TERMINATE;
            }
            return FileVisitResult.CONTINUE;
        }

        @Override
        public FileVisitResult visitFileFailed(Path file, IOException e)
                throws IOException {
            return FileVisitResult.SKIP_SUBTREE;
        }
    }

}
