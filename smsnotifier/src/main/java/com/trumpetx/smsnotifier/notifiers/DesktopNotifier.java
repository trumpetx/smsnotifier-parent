package com.trumpetx.smsnotifier.notifiers;

import javafx.application.Platform;
import javafx.geometry.Rectangle2D;
import javafx.scene.Group;
import javafx.scene.Scene;
import javafx.stage.Screen;
import javafx.stage.Stage;
import javafx.stage.StageStyle;
import javafx.util.Duration;
import org.controlsfx.control.Notifications;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;


public class DesktopNotifier implements INotifier {

    private static final Logger LOG = LoggerFactory.getLogger(DesktopNotifier.class);
    private final String title;
    private final String message;
    private Stage stage;

    public DesktopNotifier(String title, String message) {
        this.title = title;
        this.message = message;
    }

    @Override
    public void sendNotification() {
        Platform.runLater(() -> {
            if (stage != null) {
                LOG.debug("Popup window already open.");
                stage.toFront();
            } else {
                LOG.debug("Showing Popup");
                stage = new Stage(StageStyle.TRANSPARENT);
                stage.setAlwaysOnTop(true);
                stage.setResizable(false);
                stage.setScene(new Scene(new Group()));
                Rectangle2D primaryScreenBounds = Screen.getPrimary().getVisualBounds();

                //set Stage boundaries to the lower right corner of the visible bounds of the main screen
                stage.setX(primaryScreenBounds.getMinX() + primaryScreenBounds.getWidth() - 20);
                stage.setY(primaryScreenBounds.getMinY() + primaryScreenBounds.getHeight() - 20);
                stage.setWidth(1);
                stage.setHeight(1);
                stage.setOpacity(0D);
                stage.show();

                Notifications notifications = Notifications.create();
                notifications.title(title);
                notifications.hideAfter(Duration.minutes(2.0D));
                notifications.text(message);
                notifications.onAction(event -> {
                    LOG.info("Event Type: " + event.getEventType().toString());
                    stage.hide();
                    stage = null;
                });
                notifications.hideCloseButton();
                notifications.owner(stage);

                notifications.showInformation();
            }
        });
    }

}
