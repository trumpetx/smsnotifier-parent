package com.trumpetx.smsnotifier.notifiers;

public interface INotifier {

    void sendNotification() throws Exception;

}
