package com.trumpetx.smsnotifier.notifiers;

import com.trumpetx.smsnotifier.Constants;
import com.trumpetx.smsnotifier.services.SavedProperties;
import com.trumpetx.smsnotifier.services.TextManager;
import com.trumpetx.smsnotifier.util.Filter;
import com.trumpetx.smsnotifier.util.Validate;
import jodd.petite.meta.PetiteBean;
import jodd.petite.meta.PetiteInject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.net.ssl.TrustManager;
import javax.net.ssl.TrustManagerFactory;
import java.io.IOException;
import java.io.InputStream;
import java.security.KeyStore;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.security.cert.CertificateException;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

@PetiteBean
public class NotiferFactoryService {

    private static final Logger LOG = LoggerFactory.getLogger(NotiferFactoryService.class);
    private TrustManager[] trustManagers;
    @PetiteInject
    private SavedProperties properties;
    @PetiteInject
    private TextManager textManager;

    private List<INotifier> getNotifiers(String msg) throws Exception {
        List<INotifier> senders = new ArrayList<>();
        synchronized (properties) {
            if (isPhoneVerified() && properties.getBoolProperty(Constants.SMS_ENABLED_CFG)) {
                senders.add(getPhoneNotifier(msg, textManager.getText("queue.pop.notification.title")));
            }

            if (properties.getBoolProperty(Constants.DESKTOP_ENABLED_CFG)) {
                senders.add(new DesktopNotifier(textManager.getText("queue.pop.notification.title"), msg));
            }

            String email = properties.getProperty(Constants.EMAIL_CFG);
            if (Validate.email(email) && properties.getBoolProperty(Constants.EMAIL_ENABLED_CFG)) {
                senders.add(new EmailNotifier(email, textManager.getText("queue.pop.notification.title"), msg, getTrustManagers()));
            }
        }
        return senders;
    }

    public void sendNotification(String msg) throws Exception {
        for (INotifier notifier : getNotifiers(msg)) {
            notifier.sendNotification();
        }
    }

    private boolean isPhoneVerified() {
        LocalDateTime verified = properties.getTime(Constants.SMS_VERIFIED_CFG);
        return verified != null && verified.isBefore(LocalDateTime.now()) && Validate.phone(properties.getProperty(Constants.PHONE_CFG));
    }

    public INotifier getPhoneNotifier(String message, String optionalTitle) throws Exception {
        String carrier = properties.getProperty(Constants.CARRIER_CFG);
        String phone = Filter.phone(properties.getProperty(Constants.PHONE_CFG));
        if(Constants.CARRIER_EMAIL_TEMPLATES.containsKey(carrier)){
            return new EmailNotifier(String.format(Constants.CARRIER_EMAIL_TEMPLATES.get(carrier), phone), optionalTitle, message, getTrustManagers());
        } else {
            return new SmsNotifier(phone, message, getTrustManagers());
        }
    }

    private synchronized TrustManager[] getTrustManagers() throws CertificateException, NoSuchAlgorithmException, IOException, KeyStoreException {
        if(trustManagers == null) {
            InputStream truststoreInput = getClass().getResourceAsStream("/smsnotifier.ts");
            KeyStore trustStore = KeyStore.getInstance(KeyStore.getDefaultType());
            trustStore.load(truststoreInput, null);

            TrustManagerFactory trustFactory = TrustManagerFactory.getInstance(TrustManagerFactory.getDefaultAlgorithm());
            trustFactory.init(trustStore);
            trustManagers = trustFactory.getTrustManagers();
        }

        return trustManagers;
    }

}
