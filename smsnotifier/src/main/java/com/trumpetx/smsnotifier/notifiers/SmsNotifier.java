package com.trumpetx.smsnotifier.notifiers;

import jodd.http.HttpRequest;
import jodd.http.HttpResponse;
import jodd.http.net.SocketHttpConnectionProvider;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.net.SocketFactory;
import javax.net.ssl.SSLContext;
import javax.net.ssl.SSLSocket;
import javax.net.ssl.TrustManager;
import java.io.IOException;

public class SmsNotifier implements INotifier {

    private static final Logger LOG = LoggerFactory.getLogger(SmsNotifier.class);
    private static final String URL = "https://smsnotifier.trumpetx.com:8080/sms";
    private String phone;
    private String message;
    private final TrustManager[] trustManagers;

    public SmsNotifier(String phone, String message, TrustManager[] trustManagers) {
        this.phone = phone;
        this.message = message;
        this.trustManagers = trustManagers;
    }

    @Override
    public void sendNotification() throws Exception {
        HttpRequest httpRequest = HttpRequest
                .get(URL)
                .header("license", "ALPHA")
                .query("message", message)
                .query("number", phone);
        httpRequest.open(new SocketHttpConnectionProvider() {
            @Override
            protected SSLSocket createSSLSocket(String host, int port) throws IOException {
                try {
                    SSLContext sslcontext = SSLContext.getInstance("TLS");
                    sslcontext.init(null, trustManagers, null);
                    SocketFactory socketFactory = sslcontext.getSocketFactory();
                    SSLSocket sslSocket = (SSLSocket) socketFactory.createSocket(host, port);
                    return sslSocket;
                } catch (Exception ex) {
                    throw new IOException(ex);
                }
            }
        });
        HttpResponse response = httpRequest.send();
        LOG.debug("{}", response);
    }
}
