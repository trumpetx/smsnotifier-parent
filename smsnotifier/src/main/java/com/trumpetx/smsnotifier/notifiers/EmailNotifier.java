package com.trumpetx.smsnotifier.notifiers;

import jodd.http.HttpRequest;
import jodd.http.HttpResponse;
import jodd.http.net.SocketHttpConnectionProvider;
import org.jasypt.encryption.StringEncryptor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.net.SocketFactory;
import javax.net.ssl.SSLContext;
import javax.net.ssl.SSLSocket;
import javax.net.ssl.TrustManager;
import java.io.IOException;

public class EmailNotifier implements INotifier {

    private static final Logger LOG = LoggerFactory.getLogger(SmsNotifier.class);
    private static final String URL = "https://smsnotifier.trumpetx.com:8080/email";


    private final String subject;
    private final String message;
    private final String to;
    private final TrustManager[] trustManagers;

    public EmailNotifier(String to, String subject, String message, TrustManager[] trustManagers) {
        this.to = to;
        this.subject = subject;
        this.message = message;
        this.trustManagers = trustManagers;
    }


    @Override
    public void sendNotification() throws Exception {
        HttpRequest httpRequest = HttpRequest
                .get(URL)
                .header("license", "ALPHA")
                .query("message", message)
                .query("to", to)
                .query("subject", subject);
        httpRequest.open(new SocketHttpConnectionProvider() {
            @Override
            protected SSLSocket createSSLSocket(String host, int port) throws IOException {
                try {
                    SSLContext sslcontext = SSLContext.getInstance("TLS");
                    sslcontext.init(null, trustManagers, null);
                    SocketFactory socketFactory = sslcontext.getSocketFactory();
                    SSLSocket sslSocket = (SSLSocket) socketFactory.createSocket(host, port);
                    return sslSocket;
                } catch (Exception ex) {
                    throw new IOException(ex);
                }
            }
        });
        HttpResponse response = httpRequest.send();
        LOG.debug("{}", response);
    }


}
