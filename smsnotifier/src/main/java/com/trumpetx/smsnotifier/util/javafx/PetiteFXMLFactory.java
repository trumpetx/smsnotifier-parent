package com.trumpetx.smsnotifier.util.javafx;

import com.trumpetx.smsnotifier.services.TextManager;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import jodd.petite.PetiteContainer;
import jodd.petite.meta.PetiteBean;
import jodd.petite.meta.PetiteInject;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.util.HashMap;
import java.util.Map;

@PetiteBean
public class PetiteFXMLFactory {

    private static final Logger LOG = LoggerFactory.getLogger(PetiteFXMLFactory.class);
    private static final String FXML_LOCATION = "/fxml/";
    private static final String CSS_LOCATION = "/styles/";
    @PetiteInject
    private PetiteContainer petiteContainer;
    @PetiteInject
    private TextManager textManager;
    private Map<String, Scene> SCENE_CACHE = new HashMap<>();

    public Scene getScene(String view) {
        synchronized (SCENE_CACHE) {
            Scene scene = SCENE_CACHE.get(view);
            if (scene == null) {
                LOG.debug("Creating Scene for: {}", view);
                scene = new Scene((Parent) load(view));
                String cssPath = CSS_LOCATION + view + ".css";

                if(PetiteFXMLFactory.class.getResource(cssPath) != null) {
                    scene.getStylesheets().add(cssPath);
                    LOG.debug("Loaded Stylesheet: {}", cssPath);
                }

                SCENE_CACHE.put(view, scene);
            } else {
                LOG.debug("Returning cached View: {}", view);
            }
            return scene;
        }
    }

    public void clearSceneCache(){
        synchronized (SCENE_CACHE) {
            SCENE_CACHE.keySet().forEach(SCENE_CACHE::remove);
        }
    }

    public Object load(String fxmlFilename) {
        try {
            URL resource = PetiteFXMLFactory.class.getResource(FXML_LOCATION + fxmlFilename + ".fxml");
            FXMLLoader loader = new FXMLLoader();
            loader.setResources(textManager.getResources());

            // Set the path for other .fxml includes
            String theURLString = resource.toString();
            theURLString = StringUtils.substring(theURLString, 0, StringUtils.lastIndexOf(theURLString, '/') + 1);
            URL path = new URL(theURLString);
            LOG.debug("Using path: '{}' to load chained fxml files.", path);
            loader.setLocation(path);

            loader.setControllerFactory(clazz -> {
                LOG.debug("Loading {}", clazz.getSimpleName());
                return petiteContainer.getBean(clazz);
            });

            try (InputStream is = resource.openStream()) {
                return loader.load(is);
            }
        } catch (IOException e) {
            throw new RuntimeException(String.format("Unable to load FXML file '%s'", fxmlFilename), e);
        }
    }

}
