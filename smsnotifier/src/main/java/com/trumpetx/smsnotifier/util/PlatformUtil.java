package com.trumpetx.smsnotifier.util;

public class PlatformUtil {

    private static final boolean isMac = System.getProperty("os.name").toLowerCase().contains("mac");
    private static final boolean isWin = System.getProperty("os.name").toLowerCase().contains("win");

    public static boolean isMac(){
        return isMac;
    }

    public static boolean isWin(){
        return isWin;
    }
}
