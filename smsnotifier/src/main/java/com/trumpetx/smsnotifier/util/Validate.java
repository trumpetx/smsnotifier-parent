package com.trumpetx.smsnotifier.util;

import java.io.File;
import java.util.regex.Pattern;

public class Validate {

    private static final Pattern PHONE_PATTERN = Pattern.compile("^\\(?(\\d{3})\\)?[- ]?(\\d{3})[- ]?(\\d{4})$");
    private static final Pattern EMAIL_PATTERN = Pattern.compile("^[_A-Za-z0-9-\\+]+(\\.[_A-Za-z0-9-]+)*@[A-Za-z0-9-]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$");

    public static boolean phone(String phoneNumber) {
        return phoneNumber != null && PHONE_PATTERN.matcher(phoneNumber).matches();
    }

    public static boolean email(String email) {
        return email != null && EMAIL_PATTERN.matcher(email).matches();
    }

    public static boolean readableDir(File dir) {
        return readableFile(dir) && dir.isDirectory();
    }

    public static boolean writableFile(File f) {
        return f != null && f.exists() && f.canWrite();
    }

    public static boolean writableDir(File dir) {
        return writableFile(dir) && dir.isDirectory();
    }

    public static boolean readableFile(File f) {
        return f != null && f.exists() && f.canRead();
    }
}
