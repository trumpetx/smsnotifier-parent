package com.trumpetx.smsnotifier.services;

import com.trumpetx.smsnotifier.Constants;
import com.trumpetx.smsnotifier.notifiers.DesktopNotifier;
import com.trumpetx.smsnotifier.util.Validate;
import jodd.io.FileUtil;
import jodd.petite.meta.PetiteBean;
import jodd.petite.meta.PetiteInject;
import org.apache.commons.io.FileUtils;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.*;
import java.net.*;
import java.util.*;
import java.util.jar.JarEntry;
import java.util.jar.JarFile;

@PetiteBean
public class AddonService {

    private static final Logger LOG = LoggerFactory.getLogger(AddonService.class);

    public static final String ADDON_FOLDERNAME = "SMSNotifier";
    public static final String INTERFACE_ADDONS_FOLDERNAME = "Interface" + File.separator + "AddOns";
    public static final String SMS_NOTIFIER_TOC = "SMSNotifier.toc";
    private static final int DEV_VERSION = 0xBEEF;
    private static final String DEV_VERSION_STR = String.valueOf(DEV_VERSION);


    @PetiteInject
    private SavedProperties properties;

    public void installAddon(boolean currentThread) {
        if (currentThread) {
            installAddon();
        } else {
            new Thread((() -> installAddon()), "AddonInstaller").start();
        }
    }

    /**
     * This method will copy resources from the jar file of the current thread and extract it to the destination folder.
     *
     * @param jarConnection
     * @param destDir
     * @param prefix
     * @throws IOException
     */
    public void copyJarResourceToFolder(JarURLConnection jarConnection, File destDir, String prefix) throws IOException {
        JarFile jarFile = jarConnection.getJarFile();

        for (Enumeration<JarEntry> e = jarFile.entries(); e.hasMoreElements(); ) {
            JarEntry jarEntry = e.nextElement();
            String jarEntryName = jarEntry.getName();
            String jarConnectionEntryName = jarConnection.getEntryName();

            if(!jarEntryName.endsWith(".class") && jarEntryName.startsWith(prefix)) {
                String filename = (jarConnectionEntryName != null && jarEntryName.startsWith(jarConnectionEntryName)) ? jarEntryName.substring(jarConnectionEntryName.length()) : jarEntryName;
                LOG.debug("Copying: {}", filename);
                File currentFile = new File(destDir, filename);

                if (jarEntry.isDirectory()) {
                    currentFile.mkdirs();
                } else {
                    currentFile.getParentFile().mkdirs();
                    try (InputStream is = jarFile.getInputStream(jarEntry)) {
                        FileUtil.writeStream(currentFile, is);
                    }
                }
            }
        }
    }

    private boolean installAddon() {
        File addonsDir = getAddonsDir();

        if (Validate.writableDir(getAddonsDir())) {
            try {
                double currentVersion = getCurrentVersion();
                double installedVersion = getInstalledVersion();
                LOG.info("Addon installed version: {}, Current version: {}", installedVersion, currentVersion);
                if (currentVersion > installedVersion || currentVersion == DEV_VERSION || installedVersion == DEV_VERSION) {
                    File addonDir = new File(addonsDir.getAbsolutePath() + File.separator + ADDON_FOLDERNAME);
                    FileUtils.deleteDirectory(addonDir);
                    addonDir.mkdirs();
                    String decodedPath = URLDecoder.decode(AddonService.class.getProtectionDomain().getCodeSource().getLocation().getPath(), "UTF-8");

                    if(decodedPath.endsWith("jar")){
                        String urlPath = "jar:file:"+decodedPath+"!/";
                        LOG.debug("Copying jar resources from: {} / {}", decodedPath, urlPath);
                        JarURLConnection urlConnection = (JarURLConnection) new URL(urlPath).openConnection();
                        copyJarResourceToFolder(urlConnection, addonsDir, ADDON_FOLDERNAME +"/");
                    } else {
                        String filePath = decodedPath + File.separator + ADDON_FOLDERNAME;
                        LOG.debug("Copying file resources from: {}", filePath);
                        FileUtils.copyDirectory(new File(filePath), addonDir);
                    }
                    String msg = String.format("Successfully installed SMSNotifier addon v%.2f!", currentVersion);
                    new DesktopNotifier("Addon Installation", msg).sendNotification();
                    LOG.info(msg);
                } else {
                    LOG.debug("SMSNotifier addon is already installed.");
                }
                return true;
            } catch (IOException e) {
                LOG.error("Error installing SMSNotifier addon", e);
            }
        } else if (addonsDir != null) {
            LOG.error("Addons Directory ({}) is not writable.", addonsDir.getAbsolutePath());
        }
        LOG.error("Unable to install SMSNotifier addon.");
        return false;
    }

    private double getCurrentVersion() {
        try (InputStream is = AddonService.class.getResourceAsStream('/' + ADDON_FOLDERNAME + '/' + SMS_NOTIFIER_TOC)) {
            return getVersion(is);
        } catch (IOException e) {
            LOG.error("Error getting current version.", e);
        }
        return 0d;
    }

    private double getVersion(InputStream is) throws IOException {
        double version = 0d;
        BufferedReader r = new BufferedReader(new InputStreamReader(is));
        for (String line = r.readLine(); line != null; line = r.readLine()) {
            if (StringUtils.containsIgnoreCase(line, "version")) {
                String versionString = StringUtils.substringAfter(line, ":").trim();
                if(DEV_VERSION_STR.equals( versionString )){
                    return (double) DEV_VERSION;
                }
                try {
                    version = Double.parseDouble(versionString);
                } catch (NumberFormatException e) { }
                break;
            }
        }
        return version;
    }

    private double getInstalledVersion() {
        File addonsDir = getAddonsDir();
        if (addonsDir != null) {
            File addonToc = new File(addonsDir.getAbsolutePath() + File.separator + ADDON_FOLDERNAME + File.separator + SMS_NOTIFIER_TOC);
            if (Validate.readableFile(addonToc)) {
                try (InputStream is = new FileInputStream(addonToc)) {
                    return getVersion(is);
                } catch (IOException e) {
                    LOG.error("Error getting installed version.", e);
                }
            }
        }
        return 0d;
    }

    private File getAddonsDir() {
        String wowDirLoc = properties.getProperty(Constants.WOWDIR_CFG);
        try {
            if (null != wowDirLoc) {
                File addonsDir = new File(wowDirLoc + File.separator + INTERFACE_ADDONS_FOLDERNAME);
                if (!addonsDir.exists()) {
                    addonsDir.mkdirs();
                }
                if (Validate.readableDir(addonsDir)) {
                    return addonsDir;
                }
            }
        } catch (RuntimeException e) {
            LOG.warn("Unable to write to or create AddOns folder.");
        }

        LOG.info("No AddOns directory available");
        return null;
    }


}
