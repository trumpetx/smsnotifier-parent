package com.trumpetx.smsnotifier.services;

import jodd.petite.meta.PetiteBean;

import java.text.MessageFormat;
import java.util.ResourceBundle;

@PetiteBean
public class TextManager {

    private final ResourceBundle resources;

    public TextManager() {
        resources = ResourceBundle.getBundle("i8n.lang");
    }

    public String getText(String key) {
        return getText(key, new Object[0]);
    }

    public String getText(String key, Object... params) {
        String value;
        try {
            value = getResources().getString(key);
        } catch (RuntimeException e) {
            value = key;
        }
        if (params.length > 0) {
            try {
                value = MessageFormat.format(value, params);
            } catch (IllegalArgumentException e) {
            }
        }

        return value;
    }

    public ResourceBundle getResources() {
        return resources;
    }
}
