SMSNotifier = LibStub("AceAddon-3.0"):NewAddon("SMSNotifier", "AceConsole-3.0", "AceEvent-3.0")

local version = GetAddOnMetadata("SMSNotifier", "Version")
local DEBUG = version == "48879"
local options = {
    name = "SMSNotifier",
    handler = SMSNotifier,
    type = 'group',
    args = {
        toggle = {
            type = 'toggle',
            name = 'Enable',
            desc = 'Enables the Addon',
            set = function(info, val) SMSNotifier.db.global.enabled = val end,
            get = function(info) return SMSNotifier.db.global.enabled end,
        },
        queue = {
            name = "Queue Types",
            type = "group",
            args = {
                lfg = {
                    type = 'toggle',
                    name = 'LFG/LFR',
                    desc = 'Enables LFG/LFR/Scenario notifications',
                    set = function(info, val) SMSNotifier.db.global.lfg = val end,
                    get = function(info) return SMSNotifier.db.global.lfg end,
                },
                pet = {
                    type = 'toggle',
                    name = 'Pet Battles',
                    desc = 'Enables Pet Battle notifications',
                    set = function(info, val) SMSNotifier.db.global.pet = val end,
                    get = function(info) return SMSNotifier.db.global.pet end,
                },
                pvp = {
                    type = 'toggle',
                    name = 'PvP',
                    desc = 'Enables PvP notifications',
                    set = function(info, val) SMSNotifier.db.global.pvp = val end,
                    get = function(info) return SMSNotifier.db.global.pvp end,
                },
            },
        },
        role = {
            name = "Character Role",
            type = "group",
            args = {
                tank = {
                    type = 'toggle',
                    name = 'Tank',
                    desc = 'Enables notifications for the Tank role',
                    set = function(info, val) SMSNotifier.db.global.tank = val end,
                    get = function(info) return SMSNotifier.db.global.tank end,
                },
                heal = {
                    type = 'toggle',
                    name = 'Healer',
                    desc = 'Enables notifications for the Healer role',
                    set = function(info, val) SMSNotifier.db.global.heal = val end,
                    get = function(info) return SMSNotifier.db.global.heal end,
                },
                dps = {
                    type = 'toggle',
                    name = 'DPS',
                    desc = 'Enables notifications for the DPS role',
                    set = function(info, val) SMSNotifier.db.global.dps = val end,
                    get = function(info) return SMSNotifier.db.global.dps end,
                },
            },
        },
    },
}

local defaults = {
    global = {
        enabled = true,
        lfg = true,
        pvp = true,
        pet = true,
        tank = true,
        heal = true,
        dps = true,
    },
}


function SMSNotifier:OnInitialize()
    self.db = LibStub("AceDB-3.0"):New("SMSNotifierDB", defaults, true)
    LibStub("AceConfig-3.0"):RegisterOptionsTable("SMSNotifier", options)
    self.optionsFrame = LibStub("AceConfigDialog-3.0"):AddToBlizOptions("SMSNotifier", "SMSNotifier")
    self:RegisterChatCommand("sms", "ChatCommand")
    self:RegisterChatCommand("smsnotifier", "ChatCommand")
    self:Print("SMSNotifier v." .. version .. " loaded.")
end

function SMSNotifier:OnEnable()
    self:RegisterEvent("LFG_PROPOSAL_SHOW")
    self:RegisterEvent("UPDATE_BATTLEFIELD_STATUS")
    self:RegisterEvent("PET_BATTLE_QUEUE_PROPOSE_MATCH")
end

function SMSNotifier:OnDisable()
    self:Print("SMSNotifier v." .. version .. " disabled.")
end

function SMSNotifier:ChatCommand(input)
    if not input or input:trim() == "" then
        -- This is subject to a wow where the frame does not open the first time this is called, just using the Ace frame instead...
        -- InterfaceOptionsFrame_OpenToCategory(self.optionsFrame)
        LibStub("AceConfigDialog-3.0"):Open("SMSNotifier")
    elseif input:trim() == "test" then
        SMSNotifier_DoNotify("Test LFG", "lfg")
    else
        LibStub("AceConfigCmd-3.0"):HandleCommand("sms", "SMSNotifier", input:trim() ~= "help" and input or "")
    end
end


--
-- Do the notification
--
function SMSNotifier_DoNotify(type, img)
    print("SMSNotifier detected a " .. type .." pop.")

    local f = CreateFrame("Frame",nil,UIParent)
    f:SetFrameStrata("BACKGROUND")
    f:SetWidth(256)
    f:SetHeight(256)

    local t = f:CreateTexture(nil,"BACKGROUND")
    t:SetTexture("Interface/AddOns/SMSNotifier/" .. img .. ".tga")
    t:SetAllPoints(f)
    f:SetFrameStrata("TOOLTIP")
    f.texture = t

    f:SetPoint("TOPLEFT", 3, 3)
    f:Show()
    TakeScreenshot()
    C_Timer.After(.4, function()
        f:Hide()
    end)
end

--
-- StringUtil.startsWith
--
function StringUtil_startsWith(str,startsWith)
    return string.sub(str,1,string.len(startsWith))==startsWith
end

function SMSNotifier:LFG_PROPOSAL_SHOW()
    if SMSNotifier.db.global.lfg then
        local proposalExists, id, typeID, subtypeID, name, texture, role, hasResponded, nonsenseValue, completedEncounters, numMembers, isLeader, isSomethingElse, totalEncounters = GetLFGProposal();

        if(DEBUG) then
            print("typeID=" .. typeID)
            print("subtypeID=" .. subtypeID)
            print("name=" .. name)
            print("role=" .. role)
        end

        local isProvingGrounds = StringUtil_startsWith(name, "Proving Grounds")

        role = role:lower()
        local isRole = (role == "tank" and self.db.global.tank)
        isRole = isRole or (role == "healer" and self.db.global.healer)
        isRole = isRole or (role == "damager" and self.db.global.dps)

        if proposalExists and isRole and not isProvingGrounds then
            SMSNotifier_DoNotify("LFG/LFR/Scenario", "lfg")
        end
    end
end

function SMSNotifier:UPDATE_BATTLEFIELD_STATUS()
    if SMSNotifier.db.global.pvp then
        for i = 1, 3 do
            local status, mapName, instanceID = GetBattlefieldStatus(i)
            if(DEBUG) then
                mapName = mapName == nil and "" or mapName
                print("status=" .. status)
                print("mapName=" .. mapName)
                print("instanceID=" .. instanceID)
            end
            if status == "confirm" then
                SMSNotifier_DoNotify("pvp", "pvp");
                return
            end
        end
    end
end

function SMSNotifier:PET_BATTLE_QUEUE_PROPOSE_MATCH()
    if SMSNotifier.db.global.pet then
        SMSNotifier_DoNotify("pet battle", "pet")
    end
end
