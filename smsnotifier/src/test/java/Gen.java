import org.junit.Test;
import java.io.DataInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.security.KeyFactory;
import java.security.PrivateKey;
import java.security.PublicKey;
import java.security.spec.PKCS8EncodedKeySpec;
import java.security.spec.RSAPublicKeySpec;

import sun.security.rsa.RSAPrivateCrtKeyImpl;

import javax.xml.bind.DatatypeConverter;

public class Gen {

    @Test
    public void generate()throws Exception {
//convert pem to der with "openssl pkcs8 -topk8 -nocrypt -in key.pem -inform"
        byte[] privateKeyBytes = getPrivateKeyBytes(System.getProperty("user.home") + File.separator + "myserver.key.der");
        PrivateKey privateKey = getPrivate(privateKeyBytes); //get private key

//you need to know the Implementation. the interfaces doesn't have all informations or parse it out of the privateKey.toString()
        RSAPrivateCrtKeyImpl rsaPrivateKey = (RSAPrivateCrtKeyImpl)privateKey;
//create a KeySpec and let the Factory due the Rest. You could also create the KeyImpl by your own.
        PublicKey publicKey = KeyFactory.getInstance("RSA").generatePublic(new RSAPublicKeySpec(rsaPrivateKey.getModulus(), rsaPrivateKey.getPublicExponent()));
        System.out.println(DatatypeConverter.printHexBinary(publicKey.getEncoded())); //store it - that's it

    }

    public static byte[] getPrivateKeyBytes(String filename) throws Exception {
        File f = new File(filename);
        FileInputStream fis = new FileInputStream(f);
        DataInputStream dis = new DataInputStream(fis);
        byte[] keyBytes = new byte[(int) f.length()];
        dis.readFully(keyBytes);
        dis.close();

        return keyBytes;
    }

    public static PrivateKey getPrivate(byte[] privateKeyBytes)
            throws Exception {
        PKCS8EncodedKeySpec spec = new PKCS8EncodedKeySpec(privateKeyBytes);
        KeyFactory kf = KeyFactory.getInstance("RSA");
        return kf.generatePrivate(spec);
    }
}
