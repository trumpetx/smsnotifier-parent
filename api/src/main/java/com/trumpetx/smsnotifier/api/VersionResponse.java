package com.trumpetx.smsnotifier.api;

public class VersionResponse {
    public VersionResponse(Double currentVersion, String downloadUrl) {
        this.currentVersion = currentVersion;
        this.downloadUrl = downloadUrl;
    }

    private Double currentVersion;
    private String downloadUrl;

    public Double getCurrentVersion() {
        return currentVersion;
    }

    public void setCurrentVersion(Double currentVersion) {
        this.currentVersion = currentVersion;
    }

    public String getDownloadUrl() {
        return downloadUrl;
    }

    public void setDownloadUrl(String downloadUrl) {
        this.downloadUrl = downloadUrl;
    }
}
