package com.trumpetx.smsnotifier.api;

public class MessageResponse {

    private boolean messageSent;
    private String errorMessage;

    public static final MessageResponse SENT = new MessageResponse(true);

    private MessageResponse(boolean messageSent) {
        this.messageSent = messageSent;
        this.errorMessage = "";
    }

    public MessageResponse(String errorMessage) {
        this.messageSent = false;
        this.errorMessage = errorMessage;
    }

    public boolean isMessageSent() {
        return messageSent;
    }

    public void setMessageSent(boolean messageSent) {
        this.messageSent = messageSent;
    }

    public String getErrorMessage() {
        return errorMessage;
    }

    public void setErrorMessage(String errorMessage) {
        this.errorMessage = errorMessage;
    }
}
