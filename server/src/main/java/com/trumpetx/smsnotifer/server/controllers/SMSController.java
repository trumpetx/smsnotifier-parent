package com.trumpetx.smsnotifer.server.controllers;

import com.trumpetx.smsnotifier.api.MessageResponse;
import com.twilio.sdk.TwilioRestClient;
import com.twilio.sdk.TwilioRestException;
import com.twilio.sdk.resource.factory.MessageFactory;
import com.twilio.sdk.resource.instance.Message;
import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.HttpStatus;
import org.apache.http.NameValuePair;
import org.apache.http.client.HttpClient;
import org.apache.http.client.config.RequestConfig;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.utils.URIBuilder;
import org.apache.http.impl.client.HttpClientBuilder;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.util.EntityUtils;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.*;

import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.List;

@RestController
public class SMSController extends AbstractController {

    private static final String ACCOUNT_SID = "AC14ecd42daca0bd584e43a3fdb495b932";
    private static final String AUTH_TOKEN = "a2d85a00bb91b19e0a4797df75cecd2b";
    private static final String FROM = "+15129313813";
    private static final String PRD = "localhost";
    private static final String DEV = "192.168.1.74";
    private static final int TIMEOUT = 2000;
    private static final int PORT = 42896;

    @Value("${dev.mode}")
    private boolean devMode = true;

    @RequestMapping(value = "/sms", method = RequestMethod.GET)
    public MessageResponse sendSMS(@RequestHeader(value = "license") String license, @RequestParam(value = "number") String number, @RequestParam(value = "message") String message) throws TwilioRestException {
        if(!licenseService.verifyLicense(license)) {
            return new MessageResponse("Invalid License");
        }

        return sendCheapMessage(number, message) || sendTwilioMessage(number, message)
                ? MessageResponse.SENT
                : new MessageResponse("Unable to send SMS Message");
    }

    private boolean sendCheapMessage(String number, String message) {
        URI uri;
        try {
            uri = new URIBuilder()
                    .setScheme("http")
                    .setHost(devMode ? DEV : PRD)
                    .setPort(PORT)
                    .setPath("/send.html")
                    .setParameter("smsto", number)
                    .setParameter("smsbody", message)
                    .setParameter("smstype", "sms")
                    .build();

            RequestConfig.Builder requestBuilder = RequestConfig.custom();
            requestBuilder = requestBuilder.setConnectTimeout(TIMEOUT);
            requestBuilder = requestBuilder.setConnectionRequestTimeout(TIMEOUT);

            HttpClientBuilder builder = HttpClientBuilder.create();
            builder.setDefaultRequestConfig(requestBuilder.build());
            HttpClient httpclient = builder.build();
            HttpGet httpget = new HttpGet(uri);

            HttpEntity entity = null;
            try {
                HttpResponse response = httpclient.execute(httpget);
                entity = response.getEntity();
                if (response.getStatusLine().getStatusCode() != HttpStatus.SC_OK) {
                    LOG.error("Bad Status Code: {}", response.getStatusLine().toString());
                    return false;
                }
            } finally {
                EntityUtils.consume(entity);
            }
        } catch (URISyntaxException | IOException e) {
            LOG.error("Failed sending Cheap text", e);
            return false;
        }

        LOG.info("Cheap message sent to: {}", number);
        return true;
    }

    private boolean sendTwilioMessage(String number, String message) {
        TwilioRestClient client = new TwilioRestClient(ACCOUNT_SID, AUTH_TOKEN);

        List<NameValuePair> params = new ArrayList<>();
        params.add(new BasicNameValuePair("To", number));
        params.add(new BasicNameValuePair("From", FROM));
        params.add(new BasicNameValuePair("Body", message));

        MessageFactory messageFactory = client.getAccount().getMessageFactory();
        Message msg;
        try {
            msg = messageFactory.create(params);
        } catch (TwilioRestException e) {
            LOG.error("Failed sending Twilio text", e);
            return false;
        }

        LOG.debug("Twilio message sent to: {}, sid={}", number, msg.getSid());
        return true;
    }
}
