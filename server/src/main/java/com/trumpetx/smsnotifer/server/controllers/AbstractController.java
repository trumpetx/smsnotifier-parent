package com.trumpetx.smsnotifer.server.controllers;

import com.trumpetx.smsnotifer.server.services.LicenseService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

public abstract class AbstractController {

    @Autowired
    protected LicenseService licenseService;

    protected final Logger LOG = LoggerFactory.getLogger(getClass());
}
