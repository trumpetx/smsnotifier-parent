package com.trumpetx.smsnotifer.server.config;

import org.apache.catalina.connector.Connector;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.context.embedded.ConfigurableEmbeddedServletContainer;
import org.springframework.boot.context.embedded.EmbeddedServletContainerCustomizer;
import org.springframework.boot.context.embedded.tomcat.TomcatConnectorCustomizer;
import org.springframework.boot.context.embedded.tomcat.TomcatEmbeddedServletContainerFactory;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.util.ResourceUtils;

import java.io.File;
import java.io.FileNotFoundException;

@Configuration
public class ServletConfig {

    @Value("${dev.mode}")
    private boolean devMode;

    @Bean
    @Autowired
    public EmbeddedServletContainerCustomizer containerCustomizer(@Value("${keystore.file}") String keystoreFile,
                                                                  @Value("${keystore.password}") String keystorePassword,
                                                                  @Value("${keystore.type}") String keystoreType,
                                                                  @Value("${keystore.alias}") String keystoreAlias) throws FileNotFoundException {
        if(devMode) {
            return (ConfigurableEmbeddedServletContainer factory) -> {
                TomcatEmbeddedServletContainerFactory containerFactory = (TomcatEmbeddedServletContainerFactory) factory;
                containerFactory.addConnectorCustomizers((TomcatConnectorCustomizer) (Connector connector) -> {
                    connector.setSecure(false);
                    connector.setScheme("http");
                    connector.setAttribute("SSLEnabled", false);
                });
            };
        }

        final String absoluteKeystoreFile = System.getProperty("user.home") + File.separator + keystoreFile;

        return (ConfigurableEmbeddedServletContainer factory) -> {
            TomcatEmbeddedServletContainerFactory containerFactory = (TomcatEmbeddedServletContainerFactory) factory;
            containerFactory.addConnectorCustomizers((TomcatConnectorCustomizer) (Connector connector) -> {
                connector.setSecure(true);
                connector.setScheme("https");
                connector.setAttribute("keystoreFile", absoluteKeystoreFile);
                connector.setAttribute("keystorePass", keystorePassword);
                connector.setAttribute("keystoreType", keystoreType);
                connector.setAttribute("keyAlias", keystoreAlias);
                connector.setAttribute("clientAuth", "false");
                connector.setAttribute("sslProtocol", "TLS");
                connector.setAttribute("SSLEnabled", true);
            });
        };
    }
}
