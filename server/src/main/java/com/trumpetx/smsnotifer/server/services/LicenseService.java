package com.trumpetx.smsnotifer.server.services;

import org.springframework.stereotype.Service;

import java.time.LocalDate;


@Service
public class LicenseService {

    public boolean verifyLicense(String license){
        if("ALPHA".equals(license)) {
            return ! LocalDate.now().isAfter(LocalDate.of(2015, 1, 1));
        }



        return false;
    }

}
