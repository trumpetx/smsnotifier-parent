package com.trumpetx.smsnotifer.server.controllers;

import com.trumpetx.smsnotifier.api.VersionResponse;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class CurrentVersionController extends AbstractController {

    @RequestMapping(value = "/version", method = RequestMethod.GET)
    public VersionResponse version(){
        return new VersionResponse(1.4D, "https://smsnotifier.trumpetx.com/download");
    }
}
