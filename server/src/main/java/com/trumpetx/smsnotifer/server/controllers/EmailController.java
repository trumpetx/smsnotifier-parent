package com.trumpetx.smsnotifer.server.controllers;

import com.trumpetx.smsnotifier.api.MessageResponse;
import org.apache.commons.mail.DefaultAuthenticator;
import org.apache.commons.mail.Email;
import org.apache.commons.mail.SimpleEmail;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.*;

@RestController
public class EmailController extends AbstractController {

    @Value("${dev.mode}")
    private boolean devMode = true;

    @RequestMapping(value = "/email", method = RequestMethod.GET)
    public MessageResponse sendSMS(@RequestHeader(value = "license") String license, @RequestParam(value = "to") String to, @RequestParam(value = "message") String message, @RequestParam(value = "subject") String subject) {

        if(!licenseService.verifyLicense(license)) {
            return new MessageResponse("Invalid License");
        }

        Email email = new SimpleEmail();
        email.setHostName("smtp.gmail.com");
        email.setSmtpPort(465);
        email.setAuthenticator(new DefaultAuthenticator(FROM, PWRD));
        email.setSSLOnConnect(true);
        try {
            email.setFrom(FROM);
            email.setSubject(subject);
            email.setMsg(message);
            email.addTo(to);
            email.send();
        } catch (Exception e) {
            LOG.error("Error sending email.", e);
            new MessageResponse("Unable to send Email Message");
        }
        LOG.debug("Email sent to: {}", to);
        return MessageResponse.SENT;
    }

    private static final String PWRD = "gkAUgfBVccwd";
    private static final String FROM = "wowsmsnotifier@trumpetx.com";

}
